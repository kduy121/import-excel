<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('import');
});

Route::controller(UserController::class)->group(function () {
    Route::post('read-file', 'readFileExcel')->name('user.read.file');
    Route::post('update-user/{user}', 'updateUser')->name('user.update');
    Route::post('update-checked', 'updateChecked')->name('user.update.checked');
    Route::get('export-excel', 'exportExcel')->name('user.export.excel');
});
