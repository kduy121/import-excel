@extends('layout')
@section('content')
    <div class="container">
        <p style="font-size: 30px">Lists User (1)</p>
        <div class="row mb-3">
            <div class="col d-flex justify-content-between">
                <button type="button" class="btn btn-primary"
                    onclick="updateChecked('{{ route('user.update.checked') }}')">Update checked</button>
                <button type="button" class="btn btn-primary"
                    onclick="exportToExcel('{{ route('user.export.excel') }}', {{ $users }})">Export Excel</button>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">Checkbox</th>
                    <th scope="col">#</th>
                    <th scope="col">ID</th>
                    <th scope="col">Group ID</th>
                    <th scope="col">Change To</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Fax</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td style="text-align: center">
                            <input type="checkbox" name="user_ids[]" value="{{ $user->id }}">
                        </td>
                        <td> 1 </td>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->group_id }}</td>
                        <td>
                            <select class="form-select" name="change_to">
                                <option value="1" {{ $user->group_id == '1' ? 'selected' : '' }}>1</option>
                                <option value="2" {{ $user->group_id == '2' ? 'selected' : '' }}>2</option>
                                <option value="3" {{ $user->group_id == '3' ? 'selected' : '' }}>3</option>
                            </select>
                        </td>
                        <td>{{ $user->first_name }}</td>
                        <td>{{ $user->last_name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->phone }}</td>
                        <td>{{ $user->fax }}</td>
                        <td>
                            <button type="button" class="btn btn-primary"
                                onclick="updateUser('{{ route('user.update', $user->id) }}')">Update</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <script>
        function updateUser(apiUrl) {
            var selectElement = document.querySelector('select[name="change_to"]');
            var selectedValue = selectElement.value;

            console.log('selectedValue', selectedValue);

            fetch(apiUrl, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    body: JSON.stringify({
                        group_id: selectedValue
                    })
                })
                .then(response => {
                    if (response.ok) {
                        alert("Success");
                        location.reload();
                    } else {
                        alert("Failed");
                    }
                })
                .catch(error => {
                    console.error('Error:', error);
                    alert("Failed");
                });
        }

        function updateChecked(apiUrl) {
            var selectedRows = document.querySelectorAll('input[name="user_ids[]"]:checked');
            var requestData = [];

            selectedRows.forEach(function(row) {
                var userId = row.value;
                var selectElement = row.closest('tr').querySelector('select[name="change_to"]');
                var selectedValue = selectElement.value;

                requestData.push({
                    user_id: userId,
                    group_id: selectedValue
                });
            });

            console.log('requestData', requestData);

            fetch(apiUrl, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    body: JSON.stringify(requestData)
                })
                .then(response => {
                    if (response.ok) {
                        alert("Success");
                        location.reload();
                    } else {
                        alert("Failed");
                    }
                })
                .catch(error => {
                    console.error('Error:', error);
                    alert("Failed");
                });
        }

        function exportToExcel(apiUrl, data) {
            const queryString = data.map(item => `id[]=${item.id}`).join('&');
            const urlWithQuery = `${apiUrl}?${queryString}`;
            
            window.location.href = urlWithQuery;
        }
    </script>
@endsection
