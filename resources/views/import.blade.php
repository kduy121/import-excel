@extends('layout')

@section('content')
    <div class="container">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-header" style="font-size: 18px">Select files from your computer</div>
                            <br>
                            <form method="POST" action="{{ route('user.read.file') }}" enctype="multipart/form-data">
                                @csrf
                                <input type="file" class="form-control-file" id="file" name="file">
                                <button type="submit" class="btn btn-primary">Upload file</button>
                                @error('file')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
