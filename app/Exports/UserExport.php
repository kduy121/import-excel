<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UserExport implements FromCollection, WithHeadings
{
    public function __construct($data = [])
    {
        $this->data = $data;
    }

    public function collection()
    {
        return $this->data;
    }

    public function headings(): array
    {
        $fields = [
            'ID',
            'Group ID',
            'First Name',
            'Last Name',
            'Email',
            'Phone',
            'Created Date',
            'Updated Date',
        ];

        return $fields;
    }
}
