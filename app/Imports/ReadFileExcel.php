<?php

namespace App\Imports;

use App\Enums\UserType;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ReadFileExcel implements SkipsEmptyRows, ToCollection, WithHeadingRow
{
    private const GROUP_ID = UserType::GROUP_ID->value;

    private $data = [];

    public function __construct($callback)
    {
        $this->callback = $callback;
    }

    public function collection(Collection $rows)
    {
        $data = $rows->where('group_id', self::GROUP_ID)->pluck('id');
        call_user_func($this->callback, $data);
    }
}
