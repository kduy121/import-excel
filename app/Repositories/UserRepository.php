<?php

namespace App\Repositories;

use App\Enums\UserType;
use App\Models\User;

class UserRepository
{
    public function getUser(array $userId)
    {
        return User::whereIn('id', $userId)->where('group_id', UserType::GROUP_ID->value)->get();
    }

    public function updateUser(User $user, array $data)
    {
        return $user->update($data);
    }

    public function updateChecked(array $data): void
    {
        try {
            foreach ($data as $item) {
                $user = User::find($item['user_id']);
                $user->group_id = $item['group_id'];
                $user->save();
            }
        } catch (\Throwable $th) {
            throw $th;
        }

    }
}
